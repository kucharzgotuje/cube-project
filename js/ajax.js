$(document).ready(function(){
    var poprzedniaKlasa = 'Brak klasy';
    window.id = '';
    $('ul.menu li a').click(function (){	//traverse through all our navigation links..
            checkURL(this.hash);	//.. and assign them a new onclick event, using their own hash as a parameter (#page1 for example)
            id = $(this).attr('href');
            /* Animate! Take id variable from ajax.js, get rid of # and animate to it */
            animateTo = id.replace("#", "");
            /* Switch Class and trigger animation */
            $('#cube').removeClass(poprzedniaKlasa);
            $('#cube').addClass(animateTo);
            poprzedniaKlasa = animateTo;
    });
});

var lasturl="";	//here we store the current URL hash
function checkURL(hash)
{
    if(!hash) hash=window.location.hash;	//if no parameter is provided, use the hash value from the current address
    if(hash != lasturl)	// if the hash value has changed
    {
        lasturl=hash;	//update the current hash
        loadPage(hash);	// and load the new page
    }
}

function loadPage(url)	//the function that loads pages via AJAX
{
    url=url.replace('#page','');	//strip the #page part of the hash and leave only the page number
    $('#loading').css('visibility','visible');	//show the rotating gif animation
    $.ajax({	//create an ajax request to load_page.php
        type: "POST",
        url: "load_page.php",
        data: 'page='+url,	//with the page number as a parameter
        dataType: "html",	//expect html to be returned
        success: function(msg){
            if(parseInt(msg)!=0)	//if no errors
            {
                $(id + '.content_holder').html(msg).animate({
                    opacity: 1
                }, 500, function() {
                    $(this).html(msg);
                });	//load the returned html into pageContet
                $('#loading').css('visibility','hidden');	//and hide the rotating gif
            }
        }
    });
}