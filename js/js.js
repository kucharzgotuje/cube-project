$(document).ready(function(){

    function okreslWymiary() {
        var windowHeight = $('body').height(),
            windowWidth = $('body').width(),
            cubeDimension = windowHeight / 1.4,
            translateZ = cubeDimension / 2,
            autoPadding = ( cubeDimension / 600 ) * 2;

        /*Nie przekraczamy 600px */
        if ( cubeDimension >= 600 ) {
            cubeDimension = 600;
            translateZ = 300;
        } else {
            autoPadding = 0;
        }

        console.log('Padding: ' + autoPadding);
        $('.container').height(windowHeight * 0.6);
        $('.container').css('padding-top', autoPadding + '%');
        $('#cube').width(cubeDimension);
        $('#cube').height(cubeDimension);
        $('#cube figure').width(cubeDimension);
        $('#cube figure').height(cubeDimension);
        $('#cube .front').css('transform', 'rotateY(   0deg ) translateZ( ' + translateZ + 'px )');
        $('#cube .back').css('transform', 'rotateY(   180deg ) translateZ( ' + translateZ + 'px )');
        $('#cube .right').css('transform', 'rotateY(   90deg ) translateZ( ' + translateZ + 'px )');
        $('#cube .left').css('transform', 'rotateY(   -90deg ) translateZ( ' + translateZ + 'px )');
        $('#cube .top').css('transform', 'rotateX(   90deg ) translateZ( ' + translateZ + 'px )');
        $('#cube .bottom').css('transform', 'rotateX(   -90deg ) translateZ( ' + translateZ + 'px )');
    }

    okreslWymiary();
    $(window).resize( function(){
        okreslWymiary();
    });

    console.log('Ready to cube.');

    var offset = 0,
        xPos = 0, yPos = 0,
        wrapperY = $('div.wrapper').height() - 52,
        wrapperX = $('div.wrapper').width();

    $('.wajcha').draggable({
        drag: function(){
            offset = $(this).offset();
            xPos = offset.top - wrapperY;
            yPos = offset.left - wrapperX / 2;
            $( '#cube' ).css('transform', 'rotateX('+ ( - xPos ) +'deg) rotateY('+ yPos +'deg)');
            console.log(' X: ' + xPos + ' Y: ' + yPos);
        }
    });

    $('.wajcha').mousedown(function(){
        $( '#cube' ).css('animation', 'none');
    });

    $('.wajcha').mouseup(function(){
        $(this).animate({
            top: '50%',
            left: '50%'
            }, {
            duration: 400,
            specialEasing: {
                width: "linear",
                height: "easeOutBounce"
            }});
    });
});